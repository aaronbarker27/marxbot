### Marxbot
to use marxbot you will need to add the env var MARXTOKEN and supply it with a
valid access_token e.g.

```bash
export MARXTOKEN='somelongtokenstring'
```
