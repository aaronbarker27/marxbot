# marxbox for redflag.social is a quotebot by Aaron Beanfarmer Barker


import os
import re
import yaml
import random
from mastodon import Mastodon
from pathlib import Path

TAG_RE = re.compile(r'<[^>]+>')  # regex for removing tags
YAMLPATH = Path('quotes.yaml')  # setup path to yaml
quotes_dict = yaml.load(open(YAMLPATH))  # load yaml to dict
num_quotes = len(quotes_dict) - 1  # setup num_quotes var
counter = 0


def save_to_yaml_file(dict):
    with YAMLPATH.open('w') as fp:
        yaml.safe_dump(dict, fp, default_flow_style=False)  # write the dict to yaml file


def roll():
    index = random.randint(0, num_quotes)  # get a random number between 0 and the number of quotes
    global counter  # global counter to see if the list is empty
    if quotes_dict[index]['used'] is 1:  # if the number chosen has been used
        if counter <= num_quotes:  # check the counter against the number of quotes
            counter += 1  # increment the counter
        try:
            roll()  # re-roll
        except Exception as e:  # prob empty
            return "Someone tell the Admin the error: " + str(e) + " - Has occured."
        else:
            return "Someone tell the Admin yeet this list is empty."
    else:
        output_string = quotes_dict[index]['author'] + " - " + quotes_dict[index]['work'] + " - " + quotes_dict[index]['content']  # generate the output string
        quotes_dict[index]['used'] = 1  # set the used bit in the dict
        save_to_yaml_file(quotes_dict)  # save the qoute dict to yaml file
        if output_string:  # if the output string is blank
            return output_string  # return the output string
        else:
            return "error someone ping my admin"  # set output string to error


def remove_tags(text):
    return TAG_RE.sub('', text)  # use regex to remove html tags from string


def toot(output_string):
    redflagsocialmarxbot = Mastodon(
        access_token=os.environ['MARXTOKEN'],
        api_base_url='https://mastodon.redflag.social'
    )  # setup our quotebot

    redflagsocialmarxbot.toot(output_string)  # toot the quote
    temp = redflagsocialmarxbot.timeline_public()  # read the public fed timeline
    toot = dict(temp[0])  # move the first toot into its own dict
    account = dict(toot['account'])  # move the account info into its own dict

    print(account['username'] + " - " + remove_tags(toot['content']))  # print user and toot to stdout


toot(roll())  # run tool with the result of roll
